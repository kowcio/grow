package grow.controllers;



import javax.validation.Valid;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

/**
 * Admin controller.
 * @author TalentLab1
 *
 */
@Controller
public class AdminController {

	/**
	 * Main index for admin is moved tothe root url "/" of application where the jps`s views includes 		<br />
	 * generate content from the authorities roles of logged user / non logged user.						<br />		
	 * Don`t use this url.																					<br />	
	 * 
	 * @return
	 */
	//@Deprecated
	@RequestMapping(value="/adminPanel",method= RequestMethod.GET)
	public ModelAndView indexForAdmin() {
		System.out.println(" Loading Admin panel");
		ModelAndView mav = new ModelAndView("/admin/adminPanel");
		
		
		
		
		
		return mav;

	}
	
	
	/**
	 * Shows the given post 
	 * @param postNo
	 * @return
	 */
	@RequestMapping(value="/adminPanel/post/{postNo}",method= RequestMethod.GET)
	public ModelAndView showEditFormToUpdatePost(
			ModelAndView mav
) {
		
		mav.setViewName("index2");
		return mav;
		
	}
	
	/**
	 * Posts edit form for admin user, the old value can be see after editing on the same view.
	 * @param postNo
	 * @return
	 */
	@RequestMapping(value="/adminPanel/edit/{postNO}",method= RequestMethod.POST)
	public ModelAndView saveorUpdateTheEditedPostByAdmin(
			BindingResult result
			) {
		System.out.println(" Loading form after edit post");
		ModelAndView mav = new ModelAndView("/admin/editPostNo");
		
		
		return mav;
		
	}
	
	
	
}