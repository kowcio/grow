///////////////////////////////
///////////////////////////////
// FOR CHANGING THE GROW HELP 
///////////////////////////////
///////////////////////////////

$(document).ready(function(){
	  $('.helpBtn').popover({ 
	    html : true,
	    content: function() {
	    	var  va = $('.item.active').find('.container').attr('id');
	    	if (va == "G"){	        return $('.goal_help').html();        	}
	    	if (va == "R"){	        return $('.reality_help').html();    	}
	    	if (va == "O"){	        return $('.options_help').html();    	}
	    	if (va == "W"){	    	return $('.will_help').html();	    	}
	    },
	    title : function() {
	    	var  va = $('.item.active').find('.container').attr('id');
	    	if (va == "G"){	        return $('#goal_help_title').html();        	}
	    	if (va == "R"){	        return $('#reality_help_title').html();    	}
	    	if (va == "O"){	        return $('#options_help_title').html();    	}
	    	if (va == "W"){	    	return $('#will_help_title').html();	    	}
	    },
	    	    
template: '<div id="popHelpNoLogin" class="popover popHelpNoLogin">'+
'<div class="popover-inner myclass">'+
'<h3 class="popover-title"></h3>'+
'<div class="popover-content"><p></p></div></div></div>'

	  
	  });	});	

///////////////////////////////
///////////////////////////////
//FOR CHANGING THE GROW HELP - for logged in
///////////////////////////////
///////////////////////////////

$(document).ready(function(){
	  $('.growDescBtn').popover({ 
	    html : true,
	    content: function() {
    	return $('.grow_desc').html();	    	
	    },
	    title : "Tytul z js",  
template: '<div id="popHelpNoLogin" class="popover popHelpNoLogin">'+
'<div class="popover-inner myclass">'+
'<h3 class="popover-title"></h3>'+
'<div class="popover-content"><p></p></div></div></div>'

	  
	  });	});	

///////////////////////////////
///////////////////////////////
// FOR THE HELP BUTTON ON THE MAIN SIDE FOR EVERYBODY
//FOR CHANGING THE GROW HELP 
///////////////////////////////
///////////////////////////////

$(document).ready(function(){
	  $('.helpBtnMain').popover({ 
	    html : true,
	    content: function() {
	    	var  va = $('.item.active').find('.container').attr('id');
	    	if (va == "G"){	        return $('.goal_help').html();        	}
	    	if (va == "R"){	        return $('.reality_help').html();    	}
	    	if (va == "O"){	        return $('.options_help').html();    	}
	    	if (va == "W"){	    	return $('.will_help').html();	    	}
	    },
	    title : function() {
	    	var  va = $('.item.active').find('.container').attr('id');
	    	if (va == "G"){	        return $('#goal_help_title').html();        	}
	    	if (va == "R"){	        return $('#reality_help_title').html();    	}
	    	if (va == "O"){	        return $('#options_help_title').html();    	}
	    	if (va == "W"){	    	return $('#will_help_title').html();	    	}
	    },
	    	    
template: '<div id="popHelpMain" class="popover popHelpMain">'+
'<div class="popover-inner myclass">'+
'<h3 class="popover-title"></h3>'+
'<div class="popover-content"><p></p></div></div></div>'

	  
	  });	});	



///////////////////////////////
///////////////////////////////
// Making the basic grow for the new user
//Getting cookies or writing the basic one. 
///////////////////////////////
///////////////////////////////

$(document).ready(function(){
	var cookieDomain 	= 	"";
	var cookieName		=	"grow_cookie";
	var cookiePath 		= 	"/";
	var cookieExpires	=	365;//there is max value here, dunno how (days)
	var cookieVar = {				//define cookei with data for the chart
			grow_g : "goal step",
			grow_r : "reality step",
			grow_o : "options step",
			grow_w : "will step",
	} ;
	$.cookie.json = true;

	var c = $.cookie(cookieName);
	
	// if login else then Unnamed load from db ?
	
	if (	$.trim(  $("#loggedUsername").text()  ) == "Unnamed"){
	//		console.log("User is not logged in load grow from cookie");
			checkCookieAndLoadIt(c);
		}
	else{
		//console.log("User is logged in load from db or cookie.");
		
		$.ajax({
	    	type: "GET",
	    	url: $("#serverPathForJQandEtc").text()+"/growget",
	    	data: { uid : $("#loggedUserID").text() },
	    	success: function(grow){
	    	//       console.log("Success in loading the grow from db");
	    	       
	    	       for ( var i = 0, l = grow.length; i < l; i++ ) {
	    	    	   //console.log("Index = "+i+" val = " +grow[i].grow_g);
	    	    	   //console.log(	   grow[0].id	);
	    	    	   //console.log(	   grow[0].grow_g	);
	    	    	   }
	    //	       console.log("DBGrow = "+grow);
	    	    }
	    	});
		
		}
		
	

	
	
function checkCookieAndLoadIt(c){

	if(typeof c == 'undefined'){
		//console.log("we have no cookie");
		//set the cookie 
		$.cookie(cookieName, cookieVar  , {
			   expires : cookieExpires,          //expires in 365 days
	//		   path    : cookiePath,   //The value of the path attribute of the cookie 
			                           //(default:path page created the cookie  "/" for root/all pages).
			   							//default creates for context (on localhost testing)
			   domain  : cookieDomain, //The value of the domain attribute of the cookie
			                           //"" or null for localhost
			   secure  : false          //If set to true the secure attribute of the cookie
			                           //will be set and the cookie transmission will be https
			});
		//load the initial data
		loadGrowFromCookie( cookieVar );
		//show the alert
		$("#richAlert").html("Loaded new Grow");
		$("#richAlert").fadeIn(2000);
		$("#richAlert").fadeOut(2000);

	}
	else{
		//console.log("we have a grow cookie already");
		loadGrowFromCookie( $.cookie(cookieName ) );
		$("#richAlert").html("Welcome back ! ");
		$("#richAlert").fadeIn(2000);
		$("#richAlert").fadeOut(2000);
	}
};	
	

	function loadGrowFromCookie(j ){		//loadGrowFromCookie( $.cookie(cookieName) );
		//console.log ( "cookie = "+ JSON.stringify(j) );
		$("#grow_g").val(j.grow_g);
		$("#grow_r").val(j.grow_r);
		$("#grow_o").val(j.grow_o);
		$("#grow_w").val(j.grow_w);	
	};
	
	
//function for dynamic saving the cookie value of the grow (then add ajax push to server) 
	
	$("body").on("keypress", "#grow_g", function(event){
		c.grow_g = $("#grow_g").val();//	console.log("Old cookie = "+JSON.stringify(c));
		$.cookie(cookieName, c  , {});
		//c = $.cookie(cookieName );console.log("New cookie = "+JSON.stringify(c));
	});
	$("body").on("keypress", "#grow_r", function(event){
		c.grow_r = $("#grow_r").val();//	console.log("Old cookie = "+JSON.stringify(c));
		$.cookie(cookieName, c  , {});
		//c = $.cookie(cookieName );console.log("New cookie = "+JSON.stringify(c));
	});
	$("body").on("keypress", "#grow_o", function(event){
		c.grow_o = $("#grow_o").val();//	console.log("Old cookie = "+JSON.stringify(c));
		$.cookie(cookieName, c  , {});
		//c = $.cookie(cookieName );console.log("New cookie = "+JSON.stringify(c));
	});
	$("body").on("keypress", "#grow_w", function(event){
		c.grow_w = $("#grow_w").val();//	console.log("Old cookie = "+JSON.stringify(c));
		$.cookie(cookieName, c  , {});
		//c = $.cookie(cookieName );console.log("New cookie = "+JSON.stringify(c));
	});
	
	
});

///////////////////////////////
///////////////////////////////
//		STUFF loaded when the site is ready - smaller js fixes and etc
//		If part will fail, all will fail ... ;/?
///////////////////////////////
///////////////////////////////


$(document).ready(function(){
		//for the draggable idea - TODO
		//$( "#draggable" ).draggable();
		
		//default accorddion collapse
		$('#collapseOne').collapse("hide");
		
		//accordions should be closed by default on load
		$('.accordion-body').collapse("hide");
		
		//changing the style of MENU button when it is clicked,
		//need to make it for most of the buttons

		$(".btn-help").click(function() {  
			if ( $(this).hasClass("disabled") )
			{
				$(this).removeClass("disabled");
			}
			else
			{
				$(this).addClass("disabled");	
			}

		  });
		
		//check and show / hide the grow button
		if ($(".singleGrowDiv").length == 5)
    		$("#addGrowBtn").hide();
		else
			$("#addGrowBtn").show();
		
		
		
});











///////////////////////////////
///////////////////////////////
//	Dynamic	SCROLL for the help buttons 
///////////////////////////////
///////////////////////////////
/*$(function() {
    var $left = $('#sT_goal');
    var $test = $('#checkVals');
    $(window).bind('scroll', function() {
        //below line is just demo code
$('#log').text($(document).height() - (window.pageYOffset + window.innerHeight));
var q =$(document).height() - (window.pageYOffset + window.innerHeight);
var x = 200+q-1000;

$('#checkVals').text(
"Document.height  " +$(document).height() + 
"  Win.pageYOffset" + window.pageYOffset +
"  Win.innerHeight" + window.innerHeight +
"  Q= " + q +
"  X= " + x
);
        if (q < 1400) 
        {       
            if (x < 575) {$('#left').css('marginTop', 0);}
            else         {$('#left').css('marginTop', x);}
            
        } else {
            $left.css('marginTop', 0);
        }
    });
});*/





///////////////////////////////
///////////////////////////////
//		Popovers for help  
///////////////////////////////
///////////////////////////////
$(document).ready(function(){
	
  $('#helpBtnMainMain').popover({ 
    html : true,
    content: $('.grow_desc').html(),
    title : 'Grow general help',  
    template: 
    '<div id="popHelpNoLogin" class="popover popHelpNoLogin ui-widget-content">'+
    '<div class="popover-inner">'+
    '<h3 class="popover-title"></h3>'+
    '<div class="popover-content"><p></p></div></div></div>'      
  });   
  
    $('#helpBtnG').popover({ 
      html :  true,
      content: $('.goal_help').html(),
     title :$('#goal_help_title').html(),
     template: '<div id="popHelpNoLogin" class="popover popHelpNoLogin">'+
      '<div class="popover-inner">'+
      '<h3 class="popover-title"></h3>'+
      '<div class="popover-content"><p></p></div></div></div>'      
    });  

  $('#helpBtnR').popover({ 
    html :  true,
    content: $('.reality_help').html(),
   title :$('#reality_help_title').html(),
   template: '<div id="popHelpNoLogin" class="popover popHelpNoLogin">'+
    '<div class="popover-inner">'+
    '<h3 class="popover-title"></h3>'+
    '<div class="popover-content"><p></p></div></div></div>'      
  });  

  $('#helpBtnO').popover({ 
    html :  true,
    content: $('.options_help').html(),
   title :$('#options_help_title').html(),
   template: '<div id="popHelpNoLogin" class="popover popHelpNoLogin">'+
    '<div class="popover-inner">'+
    '<h3 class="popover-title"></h3>'+
    '<div class="popover-content"><p></p></div></div></div>'      
  });   

  $('#helpBtnW').popover({ 
    html :  true,
    content: $('.will_help').html(),
   title :$('#will_help_title').html(),
   template: '<div id="popHelpNoLogin" class="popover popHelpNoLogin">'+
    '<div class="popover-inner">'+
    '<h3 class="popover-title"></h3>'+
    '<div class="popover-content"><p></p></div></div></div>'      
  });   
}); 





///////////////////////////////
///////////////////////////////
//		Slider js for the menu  
///////////////////////////////
///////////////////////////////

$(document).ready(function(){
	
	var szer	=	$('#slideMenuContent').width();
	var menuText = '<h2 class="btn btn-warning btn-large">'+
	'MENU<i class="icon-chevron-down"></i>'+
	'<i class="icon-chevron-down"></i>'+
	'<i class="icon-chevron-down"></i></h2>';
	
	showMenuAsRemembered();
	
	
	//set margins according to the width
	$('#slideMenu'). css( "right", -szer );
	
	//set msg for the menu trigger
	$('#triggerMenu'). html(menuText);
	
	
//when the trigger is clicked we check to see if the popout is currently hidden
//based on the hidden we choose the correct animation
$('#triggerMenu').click( function() {
	if ($.cookie("menuIsShown") == true){
		$.cookie("menuIsShown", false);	//save menu show cookie
      hidePopout();
  }
  else {
	  $.cookie("menuIsShown", false);	//save menu show cookie
      showPopout();
  }
});

/*
 * Show slider with the menu
 */
function showPopout() {
	$.cookie("menuIsShown", true);	//save menu show cookie
	$('#slideMenu').animate({
      right: 0
  }, 'slow', function () {
      $('#triggerMenu span').html(menuText);  //change the trigger text at end of animation
  });
}

/*
 * Hide menu slider
 */
function hidePopout() {
	$.cookie("menuIsShown", false);	//save menu show cookie
	var width= $('#slideMenuContent').width();	//element width that can change
  $('#slideMenu').animate({
      right: -width
  }, 'slow', function () {
      $('#triggerMenu span').html(menuText);  //change the trigger text at end of animation
  });
}

/*
 * Set the cookie accordingly to menu click
 */
function cookieMenuShow( bool){
	if ($.cookie("menuIsShown") == null || $.cookie("menuIsShown") == undefined)
		$.cookie("menuIsShown",true);
	else
		$.cookie("menuIsShown", bool);
}

/*
 * Shows the menu as remembered by the cookies - TO CHECK
 */
function showMenuAsRemembered(){
	//console.log('menu remembered =' + $.cookie('menuIsShown'));
	if ($.cookie("menuIsShown") == true)
		showPopout();	
	else
		hidePopout();
	
}



});






///////////////////////////////
///////////////////////////////
//			Styles change  
///////////////////////////////
///////////////////////////////

$(document).ready(function(){

	loadStoredLayout();
	
	$(".lightLayout").click(function() {  
		changeLayout("light");
	});

	$(".darkLayout").click(function() {  
		changeLayout("dark");
	});
	
	//check do path fot the serwer
	function getServerPath(){
	var path = $("#serverPathForJQandEtc").text();
	//console.log("Path = " + path);
	
	if(typeof path == 'undefined' || path == null){
		path = "";
		//console.log("path undefined = "+path);
	}
	return path;
	}
	
	/*
	 *	Changing the layout if the site with cookies. 
	 */
	function changeLayout( pickedLayout){
		var path = getServerPath();
		if (pickedLayout == "light"){
			$.cookie("layoutQwas", "light", {expires:7});
			//console.log ("layout set light" + pickedLayout);
			$('#cssLayoutLink').attr('href', path+'/resources/css/all.css');
			var cssHref = $('#cssLayoutLink').attr('href');
			//console.log(cssHref);
		}
		else{
			$.cookie("layoutQwas", "dark", {expires:7});
			//console.log ("layout set dark" + pickedLayout);
			$('#cssLayoutLink').attr('href', path+'/resources/css/all2.css');
			var cssHref = $('#cssLayoutLink').attr('href');
			//console.log(cssHref);
		}	
			
		};	
/*
 * needed to change the json options for jquery cookie ib 
 */
function loadStoredLayout(){
	$.cookie.json = false;
	var lay	=	$.cookie("layoutQwas");
	var path = getServerPath();
	//console.log("remembered style = "+ lay  );
	//console.log("loadStoredLayout path = " + path);
	//console.log("loadStoredLayout path = " + path+'/resources/css/all.css');
	
	if ($.cookie("layoutQwas") == "light"){
		$('#cssLayoutLink').attr('href', path+'/resources/css/all.css');
	}
	else if ($.cookie("layoutQwas") == "dark"){
		$('#cssLayoutLink').attr('href', path+'/resources/css/all2.css');
	}
	else {
		$('#cssLayoutLink').attr('href', path+'/resources/css/all.css');
	}
	$.cookie.json = true;

};
	
	

});
	

///////////////////////////////
///////////////////////////////
//			Adding, loading , deleting grows  
///////////////////////////////
///////////////////////////////

$(document).ready(function(){
	
	//add grow button
	$("#addGrowBtn").click(function() {  
		$('#addGrowBtn').prop('disabled', true);
	console.log("clicked added grow");
		$.ajax({
	    	type: "POST",
	    	url: $("#serverPathForJQandEtc").text()+"/addnextgrow",
	    	data: { uid : $("#loggedUserID").text() },
	    	success: function(grow){
	    		console.log("200 after adding");
	    		if (grow){
    			//hide the add button when we have 3 grows (4 cause the add is in the same div)
	    		if ($(".singleGrowDiv").length == 4)
		    		$("#addGrowBtn").fadeOut(1000);
	    		blinkRichAlert("Adding grow",true);
	    		}
	    		else{
	    			blinkRichAlert("Max 4 grows !",false);
	    		}
	    		//if success add another grow change list
	    	    
	    	}
	    	});
		
//maybe update single elements - to do in future 
//var gid = $(this).closest("tr").find("td:first-child").html();

	});
	
	
	//del grow button
	$(".delGrow").bind("click",(function() {  
		console.log("clicked del grow for " );	
		//gets the id in row
//		var gid = $(this).closest("tr").find("td:first-child").html();
		var gid = $(this).closest(".singleGrowDiv").find(".growIDLoadSpan").html();
	 
		$.ajax({
	    	type: "POST",
	    	url: $("#serverPathForJQandEtc").text()+"/delgrow",
	     	data: { gid : gid},
	    	success: function(grow){
	    		console.log("succes (202) deleting the grow gid");
	    		$("#addGrowBtn").fadeIn(1000);
	    		blinkRichAlert(grow,true);
	    	    }
	    	});
	})
	);
	
	
	//load grow button
	$(".loadGrow").bind("click",(function() {  
		//console.log("clicked load grow for " );	
		//gets the id in row - always need to get parent element
var gid = $(this).closest(".singleGrowDiv").find(".growIDLoadSpan").html();
		console.log('gid = ' + gid); 

		$.ajax({
	    	type: "GET",
	    	url: $("#serverPathForJQandEtc").text()+"/loadgrow",
	     	data: { gid : gid	},
	    	success: function(j){
	    		console.log("succes (202) loading the grow");
	    		blinkRichAlert("Loading the grow", false);
	    		$("#grow_g").val(j.grow_g);
	    		$("#grow_r").val(j.grow_r);
	    		$("#grow_o").val(j.grow_o);
	    		$("#grow_w").val(j.grow_w);	
	    		$(".growNameInput").val(j.grow_name);
	    		$("#formGrowID").val(j.id);
	    		$("#formGrowUID").val(j.user_id);

	    	    }
	    	});
	})
	);
	
	/**
	 * Printing the loaded grow	printGrow 
	 */
	
	
	
	/**
	 * Blinking the rich alert when we do some action.
	 */
	function blinkRichAlert(msg, reload){
		$("#richAlert").html(msg);
		$("#richAlert").fadeIn(1000 ,
			function() {
			$("#richAlert").fadeOut(1000,
					function(){
						if (reload == true)
							location.reload(); 
					});	
			});
		
	};
	
	
});
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	




