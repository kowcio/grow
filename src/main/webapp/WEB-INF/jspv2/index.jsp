<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<!--  broken tag validation depends on the doctype, above works like a charm... -->
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<html xmlns="http://www.w3.org/1999/xhtml" lang="en">
<%@page contentType="text/html;charset=UTF-8"%>



<c:set var="path" value="${pageContext.request.contextPath}"/>
<c:set var="host" value="${pageContext.request.serverName}"/>


<head>
<title> 
Develope Your GROW and thrive !
  </title>
			
	<!-- meta tags -->
	<meta http-equiv="content-type" content="text/html; charset=UTF-8"/>
	<meta name="description" content="Strong music for a strong body"/>
	<meta name="keywords" content=", workout, music,gymradio,fitness,gym,radio"/>
	<meta name="robots" content="radio, workout, training, music, playlist, gym"/>
	<meta name="author" content="Piotr Kowalski"/>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>


<link href="<c:url value="/resources/css2/all.css"/>" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="<c:url value="/resources/js/jquery.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqueryui.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/bootstrap.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jquery.preload.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/skrypty.js"/>"></script>

<script type="text/javascript" src="<c:url value="/resources/js/analytics.js"/>"></script>



  </head>
  <body id="body">
  
  
  <!--  TOP BAR -->
 <div id="topbar">
		
				<div id="logo">
					G.R.O.W.
				</div>
				
				<ul id="menu">
					<li><a href="http://www.gymrad.io/#news">Help</a></li>
					<li><a href="http://www.gymrad.io/#news">Print</a></li>
					<li><a href="http://www.gymrad.io/#tip">About</a></li>
					<li><a href="http://www.gymrad.io/#contact">Contact</a></li>
				</ul>
					
					
</div>




<form:form method="POST" action="${path}/growadd" modelAttribute="grow">



<!-- GOAL  -->

<div id="goal">
	<div class="page">
	
		<div class="left">
			G
		</div>

		<div class="middle">
			
			<form:textarea path="grow_g" class="growtext" />
						
		</div>

		<div class="right">
         Here You have the space to describe Your GOAL.
	                <br /> Be precise, use the S.M.A.R.T. rules under help button.
	               	
	               		
        </div>
	</div>
</div>

<!-- REALITY  -->
<div id="reality">
	<div class="page">
		<div class="left">
			R
		</div>

		<div class="middle">
			
			<form:textarea path="grow_r" class="growtext" />
						
		</div>

		<div class="right">
 What is Your current reality ? Do not dream, be as much objective as possible.
              <br/>  Be honest.
             		</div>
				
			

	</div>
</div>


<!-- 	OPORTUNITY 	 -->
<div id="opportunity">
	<div class="page">
		<div class="left">
			O
		</div>

		<div class="middle">
			
			<form:textarea path="grow_o" class="growtext" />
						
		</div>

		<div class="right">
              Think about Options / Opportunities / Obstacles , describe them, use whatever You can or have.
              Write about those that can have the most impact on Your idea.
		</div>
		
	</div>
</div>



<!-- WILL TO DO -->
<div id="will">
	<div class="page">

		<div class="left">
			W
		</div>

		<div class="middle">
			
			<form:textarea path="grow_w" class="growtext" />
						
		</div>

		<div class="right">
              Make a promise to Yourself about things You will do, make a list.
              The chart table (on print) will help You measure Your progress.
		</div>
	</div>
</div>



</form:form>





<!-- footer -->
<div id="footer">
	<div class="page">
		
Footer

		
	</div>
</div>







  </body>   
  </html>
  
  
  