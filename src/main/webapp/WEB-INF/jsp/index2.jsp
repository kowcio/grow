<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

 <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>
 
<html>

<c:set var="path" value="${pageContext.request.contextPath}"/>
<c:set var="host" value="${pageContext.request.serverName}"/>


  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" /> 
    
        <!--  CSS JS JSP -->


<link href="<c:url value="/resources/css/all.css"/>" rel="stylesheet" type="text/css" />
<script type="text/javascript" src="<c:url value="/resources/js/jquery.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/bootstrap.js"/>"></script>

<script type="text/javascript" src="<c:url value="/resources/js/jquery.cookie.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqueryui.js"/>"></script>

<script type="text/javascript" src="<c:url value="/resources/js/skrypty.js"/>"></script>

    
    
    
<title> THE GROW</title>

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-44760069-1', 'worg-motivation.rhcloud.com');
  ga('send', 'pageview');

</script>
  </head>
  
  
  
  

  <body id="body">



    <!-- Part 1: Wrap all page content here -->
    <div id="wrap">
    
    
    
 <!--  HEADER -->
<jsp:include page="/WEB-INF/jsp/header/header4all.jsp"></jsp:include>
 
 
 
 
<!--  MAIN BODY --> 
 <sec:authorize access="isAuthenticated()" var="isAuthenticated" />
<c:if test="${not isAuthenticated}">
 <jsp:include page="/WEB-INF/jsp/main/main4all.jsp"></jsp:include>
</c:if>

<c:if test="${isAuthenticated}">
 <jsp:include page="/WEB-INF/jsp/main/main4logged.jsp"></jsp:include>
</c:if> 

</div>


<!--  FOOTER  -->
 <jsp:include page="/WEB-INF/jsp/footer/footer.jsp"></jsp:include>



      
  </body>
  
  
  
  
  
  


  
</html>


