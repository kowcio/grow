<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
 
 <%@ page language="java" contentType="text/html; charset=UTF-8" pageEncoding="UTF-8"%>

 
 
<c:set var="path" value="${pageContext.request.contextPath}"/>
<c:set var="host" value="${pageContext.request.serverName}"/>


    
    
  <!-- Fixed navbar -->
      <div class="navbar navbar-fixed-top">
        <div class="navbar-inner">
          <div class="container">

            
            
            <div id="greeting" class="brand">
                        <a href="${path}/" style="align:left"><i class="icon-home"></i></a>
                        <c:import url="/getUserInfo"/>  
            </div>  
            
             <!--  Login form - spring security -->
            
            <form action="${path}/j_spring_security_check" method='post'>
               <input type="text" name='j_username' class="loginInputForm"  placeholder="Login"/>
               <input type="password" name='j_password' class="loginInputForm"  placeholder="Password"/>
                <input name="submit" type="submit" value="Login" class="btn btn-success"/>
                <input name="reset" type="reset" value="Reset" class="btn btn-warning"/>    
                <a href="${path}/register/reguser" class="btn btn-success">Register</a>
       
       
                
            
<!--  i18n  -->
<jsp:include page="/WEB-INF/jsp/header/i18n.jsp"></jsp:include> 

           
            </form>
            
            
            
            

            
       
            
            
            
            
            
            
            
            
            
          </div>
        </div>
      </div>