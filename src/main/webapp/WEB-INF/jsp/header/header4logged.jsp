<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

    <!--  vars -->
    <c:set var="path" value="${pageContext.request.contextPath}"/>
    <c:set var="host" value="${pageContext.request.serverName}"/>

 



  
  
  <!-- Fixed navbar -->
      <div class="navbar navbar-fixed-top ">
        <div class="navbar-inner ">
          <div class="container ">

            
            <div id="greeting" class="brand">
                <a href="${path}/" style="align:left"><i class="icon-home"></i></a>
                  <c:import url="/getUserInfo"/>  
            </div>  
            
            
            
            
            <div class="nav-collapse collapse">
              <ul class="nav">
                
                <!--  add class active on clicking the right link - later TODO -->
                <li><a href="/"><b>G.R.O.W.</b></a></li>
                <li><a href="${path}/print">Print</a></li>
                <li><a href="${path}/about">About</a></li>
                <li><a href="${path}/contact">Contact</a></li>
                
                
                <!-- 
                <li class="dropdown">
                  <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                  Menu <b class="caret"></b></a>
                  <ul class="dropdown-menu">    
                    <li><a href="#">Add Grow</a></li>
                    <li><a href="#">Load Grow</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">5</a></li>
                    <li><a href="#">6</a></li>
                  </ul>
                </li>
                 -->

                
                
              </ul>
              <a href="<c:url value="/j_spring_security_logout" />" class="brand"> Logout  </a>
           
           

      
           <!--  LANGUAGES  -->
<jsp:include page="/WEB-INF/jsp/header/i18n.jsp"></jsp:include> 


        
        
            </div><!--/.nav-collapse -->



      
            
            
            
            
            
           
          
          </div>
        </div>
      </div>
      
      
      
      
      
      
      
      
      
      