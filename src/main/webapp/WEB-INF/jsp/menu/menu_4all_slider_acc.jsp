<%@ taglib prefix="spring" uri="http://www.springframework.org/tags" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jstl/fmt" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

    <!--  vars -->
    <c:set var="path" value="${pageContext.request.contextPath}"/>
    <c:set var="host" value="${pageContext.request.serverName}"/>

      






<!--  slider menu   -->


    
    <div id="slideMenu" class="menuIsOff">
    
    <!--  MENU button -->
    <div id="triggerMenu" class="opacityHelp">
    MENU
    </div>
    
    
    
    <div id="slideMenuContent">
    
<div class="accordion" id="accordion2">
<div class="accordion-group">

<!--  MENU  -->

<div class="accordion-heading opacityFull">
    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseMenu3">
    MENU    <i class=" icon-chevron-down"></i></a>
</div>

<div id="collapseMenu3" class="accordion-body collapse in">
<div class="accordion-inner opacityFull">


      <a class="btn btn-large btn-info btn-help"   id="helpBtnMainMain" data-placement="left" data-toggle="popover" data-container= "body">  General help </a>
      <a class="btn btn-large btn-info btn-help helpBtnG" id="helpBtnG" data-placement="left" data-toggle="popover" data-container= "body">  G  </a>
      <a class="btn btn-large btn-info btn-help helpBtnR" id="helpBtnR" data-placement="left" data-toggle="popover" data-container= "body">  R  </a>
      <a class="btn btn-large btn-info btn-help helpBtnO" id="helpBtnO" data-placement="left" data-toggle="popover" data-container= "body">  O  </a>
      <a class="btn btn-large btn-info btn-help helpBtnW" id="helpBtnW" data-placement="left" data-toggle="popover" data-container= "body">  W  </a>
      <a class="btn btn-large btn-info btn-help " href="${path}/print"> Print  </a>
      <a class="btn btn-large btn-info btn-help" href="${path}/about"> About  </a>
      <a class="btn btn-large btn-info btn-help" href="${path}/contact"> Contact  </a>
 


</div>
</div>

<!--  ChangeLog (TODO : wider) -->

<div class="accordion-heading opacityFull">
    <a class="accordion-toggle changeLogClick" data-toggle="collapse" data-parent="#accordion2" href="#collapseMenu">
    ChangeLog<i class="icon-chevron-down"></i>
    </a>
</div>
<div id="collapseMenu changeLogCollapse" class="accordion-body collapse in">

    <div class="accordion-inner opacityFull">
    Fixed UTF8 form post problem, add print button for grows, need to do some testing with scripts (maybe if time).
    </div>
    <div class="accordion-inner opacityFull">
    9.10.2013 Add grow, load grow, delete grow, fixed layout buttons layout position (in 18n),
    change save grow to get - can do f5 now without prompt. Fixning minor stuff irritating me.
    TODO : still lots of stuff - backend and security authrization issues 
    </div>
    
    <div class="accordion-inner opacityFull">
    30.09.2013 Another rearrangement of the menu ... hope it`s the last !!
    </div>
    
    <div class="accordion-inner opacityFull">
    30.09.2013 Adding changelog for the site, making some smaller improvments in code.
    </div>

</div>
</div>

<!--  About and Contact -->

<div class="accordion-heading opacityFull">
      <a class="accordion-toggle" href="${path}/contact"> Contact  </a>
</div>

<div class="accordion-heading opacityFull">
     <a class="accordion-toggle" href="${path}/about"> About  </a>
</div>


<!--  GitHub info end etc -->

<div class="accordion-heading opacityFull">
    <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2" href="#collapseMenu2">
    Repos<i class="icon-chevron-down"></i></a>
</div>
<div id="collapseMenu2" class="accordion-body collapse in">
<div class="accordion-inner opacityFull">
<ul>
    <li>
        <a href="https://bitbucket.org/kowcio/grow" 
        class="">
        Bitbucket.</a>
    </li>
</ul>
</div>
</div>



</div>
</div>
    
    
    
    </div>
    
    
    
    
    
          