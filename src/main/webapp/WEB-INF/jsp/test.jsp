<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<html>



<head>
<meta charset="utf-8" />
<title>jQuery UI Draggable - Default functionality</title>

<link href="<c:url value="/resources/css/all.css"/>" rel="stylesheet" type="text/css" />

<script type="text/javascript" src="<c:url value="/resources/js/jquery.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/bootstrap.js"/>"></script>

<script type="text/javascript" src="<c:url value="/resources/js/jquery.cookie.js"/>"></script>
<script type="text/javascript" src="<c:url value="/resources/js/jqueryui.js"/>"></script>

<script type="text/javascript" src="<c:url value="/resources/js/skrypty.js"/>"></script>


<style>
#draggable { width: 150px; height: 150px; padding: 0.5em; border:1px solid black; }
</style>
<script>



$(function() {
$( "#draggable" ).draggable();
});






</script>

</head>





<body>
<div id="draggable" class="ui-widget-content">
<p>Drag me around</p>
<div>
q<br />
q<br/>
qqqq<br/>

</div>
</div>






    <div class="dropdown">
    <a class="dropdown-toggle" data-toggle="dropdown" href="#">Dropdown trigger</a>
    <ul class="dropdown-menu">
                    <li><a href="#">1</a></li>
                    <li><a href="#">2</a></li>
                    <li><a href="#">3</a></li>
                    <li><a href="#">5</a></li>
                    <li><a href="#">6</a></li>
    </ul>
    </div>










<div class="accordion" id="accordion2">
<div class="accordion-heading">
<a class="accordion-toggle" data-toggle="collapse" 
data-parent="#accordion2" href="#collapseOne">
      <button class="btn btn-large btn-info" id="menu">MENU</button>
</a>
</div>
<div id="collapseOne" class="accordion-body collapse in">
<div class="accordion-inner">


      <a class="btn btn-large btn-info btn-help"   id="helpBtnMainMain" data-placement="left" data-toggle="popover" data-container= "body">  General help </a>
      <a class="btn btn-large btn-info btn-help helpBtnG" id="helpBtnG" data-placement="left" data-toggle="popover" data-container= "body">  G  </a>
      <a class="btn btn-large btn-info btn-help helpBtnR" id="helpBtnR" data-placement="left" data-toggle="popover" data-container= "body">  R  </a>
      <a class="btn btn-large btn-info btn-help helpBtnO" id="helpBtnO" data-placement="left" data-toggle="popover" data-container= "body">  O  </a>
      <a class="btn btn-large btn-info btn-help helpBtnW" id="helpBtnW" data-placement="left" data-toggle="popover" data-container= "body">  W  </a>
      <a class="btn btn-large btn-info btn-help " href="${path}/print"> Print  </a>
      <a class="btn btn-large btn-info btn-help" href="${path}/about"> About  </a>
      <a class="btn btn-large btn-info btn-help" href="${path}/contact"> Contact  </a>
      
      
</div>
</div>
</div>




<div id="popout" class="hidden">
    <div id="trigger"><span>Show</span></div>
    <ul>
        <li> <a href="">
            <img src="http://www.thomascsherman.com/images/so/facebook.png" /></a>

        </li>
        <li> <a href="">
            <img src="http://www.thomascsherman.com/images/so/twitter.png" /></a>

        </li>
        <li> <a href="">
            <img src="http://www.thomascsherman.com/images/so/instagram.png" /></a>

        </li>
        <li> <a href="">
            <img src="http://www.thomascsherman.com/images/so/youtube.png" /></a>

        </li>
    </ul>
</div>











<script>

//when the trigger is clicked we check to see if the popout is currently hidden
//based on the hidden we choose the correct animation
$('#trigger').click( function() {
  if ($('#popout').hasClass('hidden')) {
      $('#popout').removeClass('hidden');
      showPopout();
  }
  else {
      $('#popout').addClass('hidden');
      hidePopout();
  }
});

function showPopout() {
  $('#popout').animate({
      left: 0
  }, 'slow', function () {
      $('#trigger span').html('Close');  //change the trigger text at end of animation
  });
}

function hidePopout() {
  $('#popout').animate({
      left: -40
  }, 'slow', function () {
      $('#trigger span').html('Show');  //change the trigger text at end of animation
  });
}



</script>












</body>
</html>
